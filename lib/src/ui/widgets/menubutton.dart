part of 'widgets.dart';

class MenuButton extends StatelessWidget {
  final String title;
  final Function onTap;

  const MenuButton(this.title, {this.onTap, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 4),
            width: 125,
            // width:
            //     (MediaQuery.of(context).size.width - 2 * defaultMargin - 24) / 2,
            height: 125,
            child: Center(
              child: SizedBox(
                  height: 114,
                  child: Image(image: AssetImage(getImageFromGenre(title)))),
            ),
          ),
          Text(
            title,
            style: blackTextFont.copyWith(fontSize: 14),
          )
        ],
      ),
    );
  }

  String getImageFromGenre(String title) {
    switch (title) {
      case "Appoinment":
        return "assets/ic_appointment.png";
        break;
      case "Schedule":
        return "assets/ic_schedule.png";
        break;
      case "Upload":
        return "assets/ic_upload.png";
        break;
      case "Setup Date":
        return "assets/ic_setupdate.png";
        break;
      default:
        return "";
    }
  }
}
