part of 'pages.dart';

class HomePage extends StatefulWidget {
  final GlobalKey<ScaffoldState> globalKey;

  const HomePage({this.globalKey, Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController findTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.all(8),
          child: Row(
            children: [
              GestureDetector(
                onTap: () {
                  widget.globalKey.currentState.openDrawer();
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: Icon(
                      Icons.menu,
                      color: orangeColor,
                      size: 42,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        CarouselSlider(
          options: CarouselOptions(
            initialPage: 0,
            autoPlay: true,
            reverse: false,
            enableInfiniteScroll: true,
            viewportFraction: 1.0,
            autoPlayInterval: Duration(seconds: 5),
            autoPlayAnimationDuration: Duration(seconds: 5),
            autoPlayCurve: Curves.fastOutSlowIn,
            scrollDirection: Axis.horizontal,
          ),
          items: dummyEvent
              .map((item) => GestureDetector(
                    onTap: () {
                      // context
                      //     .read<PageBloc>()
                      //     .add(GoToEventDetailPage(item, GoToMainPage()));
                    },
                    child: Container(
                      margin: EdgeInsets.all(defaultMargin),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        child: Image.asset(item.picturePath,
                            fit: BoxFit.cover, width: 1000),
                      ),
                    ),
                  ))
              .toList(),
        ),
        Stack(
          children: [
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height / 2 + defaultMargin,
              color: Colors.white,
              child: Column(
                children: [
                  SizedBox(height: defaultMargin),
                  Wrap(
                    spacing: 16,
                    runSpacing: 16,
                    alignment: WrapAlignment.center,
                    children: [
                      MenuButton(
                        "Appoinment",
                        onTap: () {
                          context.read<PageBloc>().add(GoToAppoinmentPage(
                              GoToMainPage(bottomNavBarIndex: 0)));
                          // return;
                        },
                      ),
                      MenuButton(
                        "Schedule",
                        onTap: () {
                          context.read<PageBloc>().add(GoToSchedulePage(
                              GoToMainPage(bottomNavBarIndex: 0)));
                          // return;
                        },
                      ),
                      MenuButton(
                        "Upload",
                        onTap: () {
                          context.read<PageBloc>().add(GoToUploadPage(
                              GoToMainPage(bottomNavBarIndex: 0)));
                          // return;
                        },
                      ),
                      MenuButton(
                        "Setup Date",
                        onTap: () {
                          context.read<PageBloc>().add(GoToSetupDatePage(
                              GoToMainPage(bottomNavBarIndex: 0)));
                          // return;
                        },
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
