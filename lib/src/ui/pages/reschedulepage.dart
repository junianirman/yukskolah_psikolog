part of 'pages.dart';

class ReschedulePage extends StatefulWidget {
  final Consultation consultation;
  final PageEvent pageEvent;

  const ReschedulePage(this.consultation, this.pageEvent, {Key key})
      : super(key: key);

  @override
  _ReschedulePageState createState() => _ReschedulePageState();
}

class _ReschedulePageState extends State<ReschedulePage> {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Reschedule",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Container(
          height: 225,
          margin: EdgeInsets.all(defaultMargin),
          padding: EdgeInsets.all(defaultMargin),
          decoration: BoxDecoration(
              color: greyColor, borderRadius: BorderRadius.circular(8)),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    DateFormat('dd-MM-yyyy')
                        .format(widget.consultation.createDate),
                    style: blackTextFont,
                  ),
                  Text(
                    "Appoinment",
                    style: blackTextFont,
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                          image: AssetImage(widget.consultation.picturePath)),
                    ),
                  ),
                  Container(
                    height: 120,
                    margin: EdgeInsets.only(left: 12),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.consultation.name,
                                style: blackTextFont),
                            Text(
                                "${DateTime.now().dayName}," +
                                    DateFormat('dd-MM-yyy').format(
                                        widget.consultation.scheduleDate),
                                style: blackTextFont),
                            Text(
                                "Jam " +
                                    DateFormat.Hm().format(
                                        widget.consultation.scheduleTime) +
                                    "-" +
                                    DateFormat.Hm().format(widget
                                        .consultation.scheduleTime
                                        .add(Duration(minutes: 30))),
                                style: blackTextFont),
                            Text(widget.consultation.consultationType,
                                style: blackTextFont)
                          ],
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 2 + 16,
                          child: Divider(
                            thickness: 1,
                            color: Colors.black,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 4 + 8,
                              height: 35,
                              child: RaisedButton(
                                onPressed: () {
                                  // context
                                  //     .read<PageBloc>()
                                  //     .add(GoToMainPage(bottomNavBarIndex: 0));
                                },
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                color: darkGreyColor,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      'Date',
                                      style: blackTextFont,
                                    ),
                                    Icon(Icons.date_range)
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(width: 4),
                            Container(
                              width: MediaQuery.of(context).size.width / 4 + 8,
                              height: 35,
                              child: RaisedButton(
                                onPressed: () {
                                  // context
                                  //     .read<PageBloc>()
                                  //     .add(GoToMainPage(bottomNavBarIndex: 0));
                                },
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                color: darkGreyColor,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      'Time',
                                      style: blackTextFont,
                                    ),
                                    Icon(Icons.access_time)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 12),
              Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 4 + 8,
                    height: 35,
                    child: RaisedButton(
                      onPressed: () {
                        // context
                        //     .read<PageBloc>()
                        //     .add(GoToMainPage(bottomNavBarIndex: 0));
                      },
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16)),
                      color: darkGreyColor,
                      child: Text(
                        'Send',
                        style: blackTextFont,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
