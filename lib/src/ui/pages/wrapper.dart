part of 'pages.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!(prevPageEvent is GoToSplashPage)) {
      prevPageEvent = GoToSplashPage();
      context.watch<PageBloc>().add(prevPageEvent);
    }

    return BlocBuilder<PageBloc, PageState>(
        builder: (_, pageState) => (pageState is OnSplashPage)
            ? SplashPage()
            : (pageState is OnSignInPage)
                ? Signinpage()
                : (pageState is OnMainPage)
                    ? MainPage(bottomNavBarIndex: pageState.bottomNavBarIndex)
                    : (pageState is OnChatPage)
                        ? ChatPage(pageState.pageEvent)
                        : (pageState is OnProfileDetailPage)
                            ? ProfileDetailPage(
                                pageState.user, pageState.pageEvent)
                            : (pageState is OnAppoinmentPage)
                                ? AppoinmentPage(pageState.pageEvent)
                                : (pageState is OnReschedulePage)
                                    ? ReschedulePage(pageState.consultation,
                                        pageState.pageEvent)
                                    : (pageState is OnSchedulePage)
                                        ? SchedulePage(pageState.pageEvent)
                                        : (pageState is OnUploadPage)
                                            ? UploadPage(pageState.pageEvent)
                                            : (pageState is OnUploadFilePage)
                                                ? UploadFilePage(
                                                    pageState.pageEvent)
                                                : (pageState is OnSetupDatePage)
                                                    ? SetupDatePage(
                                                        pageState.pageEvent)
                                                    : (pageState
                                                            is OnChangePasswordPage)
                                                        ? ChangePasswordPage(
                                                            pageState.user,
                                                            pageState.pageEvent)
                                                        : (pageState
                                                                is OnSaldoPage)
                                                            ? SaldoPage(
                                                                pageState
                                                                    .pageEvent)
                                                            : (pageState
                                                                    is OnSetupDateTimePage)
                                                                ? SetupDateTimePage(
                                                                    pageState
                                                                        .pageEvent)
                                                                : Container());
  }
}
