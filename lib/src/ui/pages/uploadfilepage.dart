part of 'pages.dart';

class UploadFilePage extends StatefulWidget {
  final PageEvent pageEvent;

  UploadFilePage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _UploadFilePageState createState() => _UploadFilePageState();
}

class _UploadFilePageState extends State<UploadFilePage> {
  TextEditingController fileNameTextController = TextEditingController();
  TextEditingController descriptionTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Upload",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Container(
          margin: EdgeInsets.all(defaultMargin),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    border: Border.all(color: mainColor),
                    borderRadius: BorderRadius.circular(8)),
                child: TextField(
                  controller: fileNameTextController,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "File Name",
                      hintStyle: greyTextFont),
                ),
              ),
              SizedBox(height: defaultMargin),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    border: Border.all(color: mainColor),
                    borderRadius: BorderRadius.circular(8)),
                child: TextField(
                  maxLines: 5,
                  controller: descriptionTextController,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Description",
                      hintStyle: greyTextFont),
                ),
              ),
              SizedBox(height: defaultMargin),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2 - 32,
                    height: 40,
                    child: RaisedButton(
                      onPressed: () {},
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      color: darkGreyColor,
                      child: Text(
                        'Browse File',
                        style: blackTextFont,
                      ),
                    ),
                  ),
                  SizedBox(width: 4),
                  Container(
                    width: MediaQuery.of(context).size.width / 4,
                    height: 40,
                    child: RaisedButton(
                      onPressed: () {},
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      color: orangeColor,
                      child: Text(
                        'Save',
                        style: blackTextFont,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
