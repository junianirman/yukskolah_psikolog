part of 'pages.dart';

class SaldoPage extends StatefulWidget {
  final PageEvent pageEvent;

  SaldoPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _SaldoPageState createState() => _SaldoPageState();
}

class _SaldoPageState extends State<SaldoPage> {
  TextEditingController jumlahController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  Bank selectedBank;
  int selectedJumlah = 0;
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Penghasilan Saya",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: Column(
          children: [
            CustomTabbar(
              titles: ["Selesai Konsultasi", "Penarikan"],
              selectedIndex: selectedIndex,
              onTap: (index) {
                setState(() {
                  selectedIndex = index;
                });
              },
            ),
            Expanded(
              child: ListView(
                children: (selectedIndex == 0)
                    ? dummySaldo
                        .where(
                            (element) => element.saldoTab == SaldoTab.kosultasi)
                        .toList()
                        .map(
                          (e) => Container(
                            margin: EdgeInsets.all(defaultMargin),
                            child: Column(
                              children: [
                                Text(
                                  NumberFormat.currency(
                                          locale: "id_ID",
                                          decimalDigits: 0,
                                          symbol: "Rp ")
                                      .format(e.balance),
                                  style: blackTextFont.copyWith(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: defaultMargin,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width /
                                              3.2 +
                                          defaultMargin,
                                      height: 45,
                                      child: RaisedButton(
                                        onPressed: () {},
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        color: darkGreyColor,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              'Date',
                                              style: blackTextFont,
                                            ),
                                            Icon(Icons.date_range),
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: defaultMargin,
                                      child: Text("To", style: blackTextFont),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width /
                                              3.2 +
                                          defaultMargin,
                                      height: 45,
                                      child: RaisedButton(
                                        onPressed: () {},
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        color: darkGreyColor,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              'Date',
                                              style: blackTextFont,
                                            ),
                                            Icon(Icons.date_range),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: defaultMargin,
                                ),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height / 2,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: darkGreyColor,
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Column(
                                    children: [
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                2.4,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.all(defaultMargin),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          image: DecorationImage(
                                              image: AssetImage(e.picturePath),
                                              fit: BoxFit.cover),
                                        ),
                                      ),
                                      Text(e.description, style: blackTextFont),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                        .toList()
                    : dummySaldo
                        .where(
                            (element) => element.saldoTab == SaldoTab.penarikan)
                        .toList()
                        .map(
                          (e) => Container(
                            margin: EdgeInsets.all(defaultMargin),
                            child: Column(
                              children: [
                                Text(
                                  NumberFormat.currency(
                                          locale: "id_ID",
                                          decimalDigits: 0,
                                          symbol: "Rp ")
                                      .format(e.balance),
                                  style: blackTextFont.copyWith(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: defaultMargin,
                                ),
                                Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(color: mainColor)),
                                  child: TextField(
                                    controller: jumlahController,
                                    keyboardType: TextInputType.number,
                                    onChanged: (text) {
                                      String temp = '';
                                      for (int i = 0; i <= text.length; i++) {
                                        temp += text.isDigit(i) ? text[i] : '';
                                      }
                                      setState(() {
                                        selectedJumlah =
                                            int.tryParse(temp) ?? 0;
                                      });
                                      jumlahController.text =
                                          NumberFormat.currency(
                                                  locale: 'id_ID',
                                                  symbol: 'IDR ',
                                                  decimalDigits: 0)
                                              .format(selectedJumlah);
                                      jumlahController.selection =
                                          TextSelection.fromPosition(
                                              TextPosition(
                                                  offset: jumlahController
                                                      .text.length));
                                    },
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintStyle: greyTextFont,
                                        hintText: 'Jumlah'),
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(color: mainColor)),
                                  child: DropdownButton(
                                      value: selectedBank,
                                      hint: Text("Bank", style: greyTextFont),
                                      isExpanded: true,
                                      underline: SizedBox(),
                                      items: e.bank
                                          .map((e) => DropdownMenuItem(
                                                value: e,
                                                child: Text(
                                                  e.name,
                                                  style: blackTextFont,
                                                ),
                                              ))
                                          .toList(),
                                      onChanged: (item) {
                                        setState(() {
                                          selectedBank = item;
                                        });
                                      }),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(color: mainColor)),
                                  child: TextField(
                                    controller: accountNumberController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintStyle: greyTextFont,
                                        hintText: 'Account Number'),
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width / 2 +
                                      defaultMargin,
                                  height: 45,
                                  child: RaisedButton(
                                    onPressed: () {},
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    color: orangeColor,
                                    child: Text(
                                      'Submit',
                                      style: blackTextFont,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height / 2,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: darkGreyColor,
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Column(
                                    children: [
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                2.4,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.all(defaultMargin),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          image: DecorationImage(
                                              image: AssetImage(e.picturePath),
                                              fit: BoxFit.cover),
                                        ),
                                      ),
                                      Text(e.description, style: blackTextFont),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                        .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
