part of 'pages.dart';

class AppoinmentPage extends StatelessWidget {
  final PageEvent pageEvent;

  const AppoinmentPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Appoinment",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
          children: dummyConsultation
              .map((e) => Container(
                    height: 180,
                    margin: EdgeInsets.fromLTRB(
                      defaultMargin,
                      defaultMargin,
                      defaultMargin,
                      (dummyConsultation.indexOf(e) ==
                              dummyConsultation.length - 1
                          ? defaultMargin
                          : 0),
                    ),
                    padding: EdgeInsets.all(defaultMargin),
                    decoration: BoxDecoration(
                        color: greyColor,
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              DateFormat('dd-MM-yyyy').format(e.createDate),
                              style: blackTextFont,
                            ),
                            Text(
                              e.status == ConsultationStatus.reschedule
                                  ? "Reshedule"
                                  : e.status == ConsultationStatus.waiting
                                      ? "Waiting"
                                      : e.status ==
                                              ConsultationStatus.inschedule
                                          ? "Inschedule"
                                          : "",
                              style: e.status == ConsultationStatus.reschedule
                                  ? orangeTextFont
                                  : blackTextFont,
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          children: [
                            Container(
                              height: 120,
                              width: 120,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                image: DecorationImage(
                                    image: AssetImage(e.picturePath)),
                              ),
                            ),
                            Container(
                              height: 120,
                              margin: EdgeInsets.only(left: 12),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(e.name, style: blackTextFont),
                                      Text(
                                          "${DateTime.now().dayName}," +
                                              DateFormat('dd-MM-yyy')
                                                  .format(e.scheduleDate),
                                          style: blackTextFont),
                                      Text(
                                          "Jam " +
                                              DateFormat.Hm()
                                                  .format(e.scheduleTime) +
                                              "-" +
                                              DateFormat.Hm().format(e
                                                  .scheduleTime
                                                  .add(Duration(minutes: 30))),
                                          style: blackTextFont),
                                    ],
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width /
                                              2 +
                                          16,
                                      child: (e.status ==
                                              ConsultationStatus.reschedule)
                                          ? Divider(
                                              thickness: 1,
                                              color: Colors.black,
                                            )
                                          : SizedBox()),
                                  (e.status == ConsultationStatus.reschedule)
                                      ? Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text("Waiting for User Agree",
                                                style: blackTextFont),
                                            Text(
                                              "${DateTime.now().dayName}," +
                                                  DateFormat('dd-MM-yyy')
                                                      .format(e.scheduleDate),
                                            ),
                                            Text(
                                              "Jam " +
                                                  DateFormat.Hm()
                                                      .format(e.scheduleTime) +
                                                  "-" +
                                                  DateFormat.Hm().format(e
                                                      .scheduleTime
                                                      .add(Duration(
                                                          minutes: 30))),
                                            ),
                                          ],
                                        )
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      4 +
                                                  8,
                                              height: 35,
                                              child: RaisedButton(
                                                onPressed: () {
                                                  // context
                                                  //     .read<PageBloc>()
                                                  //     .add(GoToMainPage(bottomNavBarIndex: 0));
                                                },
                                                elevation: 0,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                ),
                                                color: darkGreyColor,
                                                child: Text(
                                                  'Accept',
                                                  style: blackTextFont,
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: 4),
                                            Container(
                                              width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      4 +
                                                  8,
                                              height: 35,
                                              child: RaisedButton(
                                                onPressed: () {
                                                  context.read<PageBloc>().add(
                                                        GoToReschedulePage(
                                                          e,
                                                          GoToAppoinmentPage(
                                                              pageEvent),
                                                        ),
                                                      );
                                                },
                                                elevation: 0,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                ),
                                                color: darkGreyColor,
                                                child: Text(
                                                  'Reschedule',
                                                  style: blackTextFont,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }
}
