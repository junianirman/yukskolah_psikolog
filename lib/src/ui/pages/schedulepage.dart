part of 'pages.dart';

class SchedulePage extends StatelessWidget {
  final PageEvent pageEvent;

  const SchedulePage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Schedule",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: ListView(
            children: dummyConsultation
                .where((element) =>
                    element.status == ConsultationStatus.inschedule)
                .map(
                  (e) => Container(
                    height: 225,
                    margin: EdgeInsets.fromLTRB(
                      defaultMargin,
                      defaultMargin,
                      defaultMargin,
                      (dummyConsultation.indexOf(e) ==
                              dummyConsultation.length - 1
                          ? defaultMargin
                          : 0),
                    ),
                    padding: EdgeInsets.all(defaultMargin),
                    decoration: BoxDecoration(
                        color: greyColor,
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              DateFormat('dd-MM-yyyy').format(e.createDate),
                              style: blackTextFont,
                            ),
                            Text(
                              e.status == ConsultationStatus.reschedule
                                  ? "Reshedule"
                                  : e.status == ConsultationStatus.waiting
                                      ? "Waiting"
                                      : e.status ==
                                              ConsultationStatus.inschedule
                                          ? "Inschedule"
                                          : "",
                              style: e.status == ConsultationStatus.reschedule
                                  ? orangeTextFont
                                  : blackTextFont,
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          children: [
                            Container(
                              height: 120,
                              width: 120,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                image: DecorationImage(
                                    image: AssetImage(e.picturePath)),
                              ),
                            ),
                            Container(
                              height: 120,
                              margin: EdgeInsets.only(left: 12),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(e.name, style: blackTextFont),
                                  Text(
                                      "${DateTime.now().dayName}," +
                                          DateFormat('dd-MM-yyy')
                                              .format(e.scheduleDate),
                                      style: blackTextFont),
                                  Text(
                                      "Jam " +
                                          DateFormat.Hm()
                                              .format(e.scheduleTime) +
                                          "-" +
                                          DateFormat.Hm().format(e.scheduleTime
                                              .add(Duration(minutes: 30))),
                                      style: blackTextFont),
                                  Text(e.consultationType, style: blackTextFont)
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 12),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 4 + 8,
                              height: 35,
                              child: RaisedButton(
                                onPressed: () {
                                  context.read<PageBloc>().add(
                                      GoToReschedulePage(
                                          e, GoToSchedulePage(pageEvent)));
                                },
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                color: darkGreyColor,
                                child: Text(
                                  'Reschedule',
                                  style: blackTextFont,
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 4 + 8,
                              height: 35,
                              child: RaisedButton(
                                onPressed: () {
                                  // context
                                  //     .read<PageBloc>()
                                  //     .add(GoToMainPage(bottomNavBarIndex: 0));
                                },
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                color: darkGreyColor,
                                child: Text(
                                  'Kirim Form',
                                  style: blackTextFont,
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 4 + 8,
                              height: 35,
                              child: RaisedButton(
                                onPressed: () {
                                  context.read<PageBloc>().add(GoToChatPage(
                                      GoToSchedulePage(pageEvent)));
                                },
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                color: darkGreyColor,
                                child: Text(
                                  'Chat',
                                  style: blackTextFont,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
                .toList()),
      ),
    );
  }
}
