part of 'pages.dart';

class Signinpage extends StatefulWidget {
  Signinpage({Key key}) : super(key: key);

  @override
  _SigninpageState createState() => _SigninpageState();
}

class _SigninpageState extends State<Signinpage> {
  TextEditingController usernameTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();

  bool isSignin = false;
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      backColor: mainColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
            child: Text(
              "Username",
              style: blackTextFont,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.circular(8)),
            child: TextField(
              controller: usernameTextController,
              decoration: InputDecoration(border: InputBorder.none),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              "Password",
              style: blackTextFont,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.circular(8)),
            child: TextField(
              obscureText: true,
              controller: passwordTextController,
              decoration: InputDecoration(border: InputBorder.none),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            height: 50,
            margin: EdgeInsets.symmetric(vertical: defaultMargin),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    child: Text(
                      "Forgot Password ?",
                      style: blackTextFont,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    height: 45,
                    padding: EdgeInsets.only(left: defaultMargin),
                    child: isSignin
                        ? loadingIndicator
                        : RaisedButton(
                            onPressed: () {
                              context
                                  .read<PageBloc>()
                                  .add(GoToMainPage(bottomNavBarIndex: 0));
                            },
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32)),
                            color: accentColor2,
                            child: Text(
                              'Login',
                              style: linkTextFont.copyWith(
                                  fontStyle: FontStyle.normal,
                                  decoration: TextDecoration.none),
                            ),
                          ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            height: 50,
            margin: EdgeInsets.symmetric(vertical: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Don\'t have account ? ",
                  style: blackTextFont,
                ),
                Text(
                  "Register Now",
                  style: linkTextFont,
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            height: 50,
            margin: EdgeInsets.symmetric(vertical: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/ic_facebook_edit.png'),
                        fit: BoxFit.cover),
                  ),
                ),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/ic_instagram_edit.png'),
                        fit: BoxFit.cover),
                  ),
                ),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/ic_google_edit.png'),
                        fit: BoxFit.cover),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.6,
            height: 40,
            margin: EdgeInsets.symmetric(vertical: defaultMargin),
            decoration: BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.circular(8)),
            child: CustomTabbarLanguage(
              titles: ['English', 'Bahasa'],
              selectedIndex: selectedIndex,
              onTap: (index) {
                setState(() {
                  selectedIndex = index;
                });
              },
            ),
          )
        ],
      ),
    );
  }
}
