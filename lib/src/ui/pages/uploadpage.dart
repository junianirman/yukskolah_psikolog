part of 'pages.dart';

class UploadPage extends StatelessWidget {
  final PageEvent pageEvent;

  const UploadPage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Upload",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: Stack(
          children: [
            Container(
              height: 200,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                  color: greyColor, borderRadius: BorderRadius.circular(8)),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Form Name",
                        style: blackTextFont,
                        overflow: TextOverflow.clip,
                        maxLines: 1,
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width / 4),
                      Text(
                        "Description",
                        style: blackTextFont,
                        overflow: TextOverflow.clip,
                        maxLines: 1,
                      ),
                    ],
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Divider(
                      thickness: 1,
                      color: Colors.black,
                    ),
                  ),
                  Column(
                    children: dummyUpload
                        .map((e) => Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 4,
                                    child: Text(
                                      e.name,
                                      style: blackTextFont,
                                      overflow: TextOverflow.clip,
                                      maxLines: 1,
                                    ),
                                  ),
                                  SizedBox(
                                      width: MediaQuery.of(context).size.width /
                                              6 +
                                          8),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 4,
                                    child: Text(
                                      e.description,
                                      style: blackTextFont,
                                      overflow: TextOverflow.clip,
                                      maxLines: 1,
                                    ),
                                  )
                                ]))
                        .toList(),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.all(defaultMargin),
                child: FloatingActionButton(
                  onPressed: () {
                    context
                        .read<PageBloc>()
                        .add(GoToUploadFilePage(GoToUploadPage(pageEvent)));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('assets/ic_plus.png'))),
                  ),
                  elevation: 0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
