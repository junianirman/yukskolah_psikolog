part of 'pages.dart';

class MainPage extends StatefulWidget {
  final int bottomNavBarIndex;

  MainPage({this.bottomNavBarIndex = 0, Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int bottomNavBarIndex;
  PageController pageController;
  GlobalKey<ScaffoldState> _mainPageScaffoldKey = new GlobalKey();

  @override
  void initState() {
    super.initState();
    bottomNavBarIndex = widget.bottomNavBarIndex;
    pageController = PageController(initialPage: bottomNavBarIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _mainPageScaffoldKey,
      backgroundColor: Color(0xFFF6F7F9),
      drawer: createCustomSideNavBar(),
      body: Stack(
        children: [
          Container(
            color: mainColor,
          ),
          PageView(
            controller: pageController,
            onPageChanged: (index) {
              setState(() {
                bottomNavBarIndex = index;
              });
            },
            children: [
              HomePage(globalKey: _mainPageScaffoldKey),
              InboxPage(),
              ProfilePage()
            ],
          ),
          createCustomBottomNavBar(),
        ],
      ),
    );
  }

  Widget createCustomBottomNavBar() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 60,
        padding: EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          color: darkGreyColor,
        ),
        child: BottomNavigationBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedItemColor: mainColor,
          unselectedItemColor: Color(0xFFE5E5E5),
          currentIndex: bottomNavBarIndex,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          selectedLabelStyle:
              GoogleFonts.raleway(fontSize: 13, fontWeight: FontWeight.w600),
          unselectedLabelStyle: blackTextFont.copyWith(
              fontSize: 13, fontWeight: FontWeight.w600, color: Colors.black),
          onTap: (index) {
            setState(() {
              bottomNavBarIndex = index;
              pageController.jumpToPage(index);
            });
          },
          items: [
            BottomNavigationBarItem(
              label: "",
              icon: Text(
                "Home",
                style: blackTextFont.copyWith(
                    fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            BottomNavigationBarItem(
              label: "",
              icon: Text(
                "Chat",
                style: blackTextFont.copyWith(
                    fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            BottomNavigationBarItem(
              label: "",
              icon: Text(
                "Profile",
                style: blackTextFont.copyWith(
                    fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
      // ),
    );
  }

  Widget createCustomSideNavBar() {
    return Drawer(
      child: Container(
        height: MediaQuery.of(context).size.height / 2,
        child: ListView(
          children: [
            ListTile(
              title: Text(
                "Psikolog Menu",
                style: blackTextFont.copyWith(
                    fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Divider(thickness: 1, color: Colors.black),
            ),
            ListTile(
              title: Text(
                "Appoinment",
                style: blackTextFont,
              ),
              onTap: () {
                context.read<PageBloc>().add(
                    GoToAppoinmentPage(GoToMainPage(bottomNavBarIndex: 0)));
              },
            ),
            ListTile(
              title: Text(
                "Schedule",
                style: blackTextFont,
              ),
              onTap: () {
                context
                    .read<PageBloc>()
                    .add(GoToSchedulePage(GoToMainPage(bottomNavBarIndex: 0)));
              },
            ),
            ListTile(
              title: Text(
                "Upload",
                style: blackTextFont,
              ),
              onTap: () {
                context
                    .read<PageBloc>()
                    .add(GoToUploadPage(GoToMainPage(bottomNavBarIndex: 0)));
              },
            ),
            ListTile(
              title: Text(
                "Setup Date",
                style: blackTextFont,
              ),
              onTap: () {
                context
                    .read<PageBloc>()
                    .add(GoToSetupDatePage(GoToMainPage(bottomNavBarIndex: 0)));
              },
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Divider(thickness: 1, color: Colors.black),
            ),
            ListTile(
              title: Text(
                "Profile",
                style: blackTextFont,
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                "About",
                style: blackTextFont,
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}
