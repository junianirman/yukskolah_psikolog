part of 'pages.dart';

class ProfilePage extends StatelessWidget {
  final PageEvent pageEvent;

  const ProfilePage({this.pageEvent, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: GestureDetector(
                onTap: () {
                  context.read<PageBloc>().add(GoToProfileDetailPage(
                      dummyUser, GoToMainPage(bottomNavBarIndex: 2)));
                },
                child: Row(
                  children: [
                    Container(
                      width: 90,
                      height: 90,
                      margin: EdgeInsets.symmetric(horizontal: 12),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: AssetImage(dummyUser.picturePath),
                            fit: BoxFit.cover),
                      ),
                    ),
                    Container(
                      height: 90,
                      width: MediaQuery.of(context).size.width / 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            dummyUser.name,
                            style: blackTextFont.copyWith(
                                fontWeight: FontWeight.bold),
                            maxLines: 1,
                            overflow: TextOverflow.clip,
                          ),
                          SizedBox(height: 8),
                          Row(
                            children: [
                              Text(
                                "Pengikut ${dummyUser.follow.follower}",
                                style: blackTextFont,
                              ),
                              SizedBox(width: 8),
                              Text(
                                "|",
                                style: blackTextFont,
                              ),
                              SizedBox(width: 8),
                              Text(
                                "Mengikuti ${dummyUser.follow.following}",
                                style: blackTextFont,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 50),
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: GestureDetector(
                onTap: () {
                  context
                      .read<PageBloc>()
                      .add(GoToSaldoPage(GoToMainPage(bottomNavBarIndex: 2)));
                },
                child: ListTile(
                  leading: Text("Saldo Psikolog", style: blackTextFont),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
