part of 'pages.dart';

class SetupDatePage extends StatelessWidget {
  final PageEvent pageEvent;

  const SetupDatePage(this.pageEvent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ThemeBloc>(context)
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: mainColor)));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Setup Date",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(pageEvent);
        },
        child: Stack(
          children: [
            Container(
              height: 200,
              margin: EdgeInsets.all(defaultMargin),
              padding: EdgeInsets.all(defaultMargin),
              decoration: BoxDecoration(
                  color: greyColor, borderRadius: BorderRadius.circular(8)),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Day",
                        style: blackTextFont,
                        overflow: TextOverflow.clip,
                        maxLines: 1,
                      ),
                      SizedBox(
                          width: MediaQuery.of(context).size.width / 2 - 48),
                      Text(
                        "Time",
                        style: blackTextFont,
                        overflow: TextOverflow.clip,
                        maxLines: 1,
                      ),
                    ],
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Divider(
                      thickness: 1,
                      color: Colors.black,
                    ),
                  ),
                  Column(
                    children: dummySetupDate
                        .map((e) => Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 4 +
                                    defaultMargin,
                                child: Text(
                                  "${e.day.dayName}",
                                  style: blackTextFont,
                                  overflow: TextOverflow.clip,
                                  maxLines: 1,
                                ),
                              ),
                              SizedBox(
                                  width: MediaQuery.of(context).size.width / 6),
                              Container(
                                width: MediaQuery.of(context).size.width / 4 +
                                    defaultMargin,
                                child: Text(
                                  DateFormat.Hm().format(e.startTime) +
                                      "-" +
                                      DateFormat.Hm().format(e.endTime),
                                  style: blackTextFont,
                                  overflow: TextOverflow.clip,
                                  maxLines: 1,
                                ),
                              )
                            ]))
                        .toList(),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.all(defaultMargin),
                child: FloatingActionButton(
                  onPressed: () {
                    context.read<PageBloc>().add(
                        GoToSetupDateTimePage(GoToSetupDatePage(pageEvent)));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('assets/ic_plus.png'))),
                  ),
                  elevation: 0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
