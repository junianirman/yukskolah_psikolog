part of 'pages.dart';

class SetupDateTimePage extends StatefulWidget {
  final PageEvent pageEvent;

  SetupDateTimePage(this.pageEvent, {Key key}) : super(key: key);

  @override
  _SetupDateTimePageState createState() => _SetupDateTimePageState();
}

class _SetupDateTimePageState extends State<SetupDateTimePage> {
  TextEditingController startTextController = TextEditingController();
  TextEditingController endTextController = TextEditingController();

  List<DateTime> dates;
  DateTime selectedDate;
  bool isSelected = false;

  TimeOfDay timeOfDay = TimeOfDay.now();

  @override
  void initState() {
    super.initState();
    dates =
        List.generate(7, (index) => DateTime.now().add(Duration(days: index)));
    // selectedDate = dates[0];
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Setup Date",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Row(
                children: [
                  Text(
                    "Start From",
                    style: blackTextFont,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextField(
                controller: startTextController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "00:00:00",
                    hintStyle: blackTextFont.copyWith(fontSize: 48)),
                onTap: () {
                  timePicker();
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Row(
                children: [
                  Text(
                    "End",
                    style: blackTextFont,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextField(
                controller: endTextController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "00:00:00",
                    hintStyle: blackTextFont.copyWith(fontSize: 48)),
              ),
            ),
            Container(
              margin: EdgeInsets.all(defaultMargin),
              child: Divider(
                color: Colors.black,
                thickness: 2,
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 3.2,
              child: ListView(
                children: dates
                    .map(
                      (e) => Container(
                        height: 32,
                        child: CheckboxListTile(
                            title: Text(e.dayName, style: blackTextFont),
                            controlAffinity: ListTileControlAffinity.leading,
                            value: isSelected,
                            onChanged: (value) {
                              setState(() {
                                isSelected = value;
                                selectedDate = e;
                              });
                            }),
                      ),
                    )
                    .toList(),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                  width: MediaQuery.of(context).size.width / 3.2,
                  height: 45,
                  child: RaisedButton(
                    onPressed: () {},
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    color: orangeColor,
                    child: Text(
                      'Save',
                      style: blackTextFont,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  timePicker() {
    showTimePicker(
      initialTime: timeOfDay,
      context: context,
      builder: (context, child) {
        return Theme(
          data: ThemeData(primarySwatch: materialMainColor),
          child: MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
            child: child,
          ),
        );
      },
    ).then((time) {
      setState(() {
        timeOfDay = time;
        startTextController.text = timeOfDay.format(context);
      });
    });
  }
}
