part of 'pages.dart';

class ChangePasswordPage extends StatefulWidget {
  final User user;
  final PageEvent pageEvent;

  ChangePasswordPage(this.user, this.pageEvent, {Key key}) : super(key: key);

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(widget.pageEvent);
        return;
      },
      child: GeneralPage(
        title: "Profile",
        onBackButtonPressed: () {
          context.read<PageBloc>().add(widget.pageEvent);
        },
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  width: 150,
                  height: 150,
                  margin: EdgeInsets.symmetric(vertical: 32),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(widget.user.picturePath),
                        fit: BoxFit.cover),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: defaultMargin, vertical: 4),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Change Password",
                      style:
                          blackTextFont.copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.fromLTRB(
                      defaultMargin, defaultMargin, defaultMargin, 0),
                  decoration: BoxDecoration(
                    border: Border.all(color: mainColor),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: TextField(
                    controller: oldPasswordController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 8, horizontal: defaultMargin),
                      border: InputBorder.none,
                      hintText: "Old Password",
                      hintStyle: blackTextFont,
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.fromLTRB(
                      defaultMargin, defaultMargin, defaultMargin, 0),
                  decoration: BoxDecoration(
                    border: Border.all(color: mainColor),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: TextField(
                    controller: newPasswordController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 8, horizontal: defaultMargin),
                      border: InputBorder.none,
                      hintText: "New Password",
                      hintStyle: blackTextFont,
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.fromLTRB(
                      defaultMargin, defaultMargin, defaultMargin, 0),
                  decoration: BoxDecoration(
                    border: Border.all(color: mainColor),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: TextField(
                    controller: confirmPasswordController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 8, horizontal: defaultMargin),
                      border: InputBorder.none,
                      hintText: "Confirm Password",
                      hintStyle: blackTextFont,
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2 - defaultMargin,
                  margin: EdgeInsets.only(top: 24),
                  height: 45,
                  padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: RaisedButton(
                    onPressed: () {},
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    color: orangeColor,
                    child: Text(
                      'Save',
                      style: blackTextFont.copyWith(fontSize: 18),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
