part of 'shared.dart';

const double defaultMargin = 16;

Color mainColor = Color(0xFF33C9EA);
Color accentColor1 = Color(0xFF025AB4);
Color accentColor2 = Color(0xFFFAC303);
Color greyColor = Color(0xFFE5E5E5);
Color darkGreyColor = Color(0xFFC4C4C4);
Color orangeColor = Color(0xFFF4B206);
Color greenColor = Color(0xFF0E740C);
Color linkColor = Color(0xFF1E0BFD);
MaterialColor materialMainColor = MaterialColor(
  0xFF33C9EA,
  <int, Color>{
    50: Color(0xFF33C9EA),
    100: Color(0xFF33C9EA),
    200: Color(0xFF33C9EA),
    300: Color(0xFF33C9EA),
    400: Color(0xFF33C9EA),
    500: Color(0xFF33C9EA),
    600: Color(0xFF33C9EA),
    700: Color(0xFF33C9EA),
    800: Color(0xFF33C9EA),
    900: Color(0xFF33C9EA),
  },
);

TextStyle blackTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.black, fontWeight: FontWeight.w500);
TextStyle whiteTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.white, fontWeight: FontWeight.w500);
TextStyle blueTextFont = GoogleFonts.raleway()
    .copyWith(color: mainColor, fontWeight: FontWeight.w500);
TextStyle greyTextFont = GoogleFonts.raleway()
    .copyWith(color: darkGreyColor, fontWeight: FontWeight.w500);
TextStyle orangeTextFont = GoogleFonts.raleway()
    .copyWith(color: orangeColor, fontWeight: FontWeight.w500);
TextStyle linkTextFont = GoogleFonts.raleway().copyWith(
    color: linkColor,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.italic,
    decoration: TextDecoration.underline);

TextStyle whiteNumberFont =
    GoogleFonts.openSans().copyWith(color: Colors.white);
TextStyle yellowNumberFont =
    GoogleFonts.openSans().copyWith(color: accentColor2);

Widget loadingIndicator = SpinKitFadingCircle(size: 45, color: mainColor);
