import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:yukskolah_psikolog/src/models/models.dart';

part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  PageBloc() : super(OnInitialPage());

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GoToSplashPage) {
      yield OnSplashPage();
    } else if (event is GoToSignInPage) {
      yield OnSignInPage();
    } else if (event is GoToMainPage) {
      yield OnMainPage(bottomNavBarIndex: event.bottomNavBarIndex);
    } else if (event is GoToChatPage) {
      yield OnChatPage(event.pageEvent);
    } else if (event is GoToProfileDetailPage) {
      yield OnProfileDetailPage(event.user, event.pageEvent);
    } else if (event is GoToAppoinmentPage) {
      yield OnAppoinmentPage(event.pageEvent);
    } else if (event is GoToReschedulePage) {
      yield OnReschedulePage(event.consultation, event.pageEvent);
    } else if (event is GoToSchedulePage) {
      yield OnSchedulePage(event.pageEvent);
    } else if (event is GoToUploadPage) {
      yield OnUploadPage(event.pageEvent);
    } else if (event is GoToUploadFilePage) {
      yield OnUploadFilePage(event.pageEvent);
    } else if (event is GoToSetupDatePage) {
      yield OnSetupDatePage(event.pageEvent);
    } else if (event is GoToChangePasswordPage) {
      yield OnChangePasswordPage(event.user, event.pageEvent);
    } else if (event is GoToSaldoPage) {
      yield OnSaldoPage(event.pageEvent);
    } else if (event is GoToSetupDateTimePage) {
      yield OnSetupDateTimePage(event.pageEvent);
    }
  }
}
