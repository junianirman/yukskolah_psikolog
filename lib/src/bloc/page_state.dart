part of 'page_bloc.dart';

abstract class PageState extends Equatable {
  const PageState();
}

class OnInitialPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnSplashPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnSignInPage extends PageState {
  @override
  List<Object> get props => [];
}

class OnMainPage extends PageState {
  final int bottomNavBarIndex;

  OnMainPage({this.bottomNavBarIndex = 0});

  @override
  List<Object> get props => [bottomNavBarIndex];
}

class OnChatPage extends PageState {
  final PageEvent pageEvent;

  OnChatPage(this.pageEvent);

  @override
  List<Object> get props => [];
}

class OnProfileDetailPage extends PageState {
  final User user;
  final PageEvent pageEvent;

  OnProfileDetailPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class OnAppoinmentPage extends PageState {
  final PageEvent pageEvent;

  OnAppoinmentPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnReschedulePage extends PageState {
  final Consultation consultation;
  final PageEvent pageEvent;

  OnReschedulePage(this.consultation, this.pageEvent);

  @override
  List<Object> get props => [consultation, pageEvent];
}

class OnSchedulePage extends PageState {
  final PageEvent pageEvent;

  OnSchedulePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnUploadPage extends PageState {
  final PageEvent pageEvent;

  OnUploadPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnUploadFilePage extends PageState {
  final PageEvent pageEvent;

  OnUploadFilePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnSetupDatePage extends PageState {
  final PageEvent pageEvent;

  OnSetupDatePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnChangePasswordPage extends PageState {
  final User user;
  final PageEvent pageEvent;

  OnChangePasswordPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class OnSaldoPage extends PageState {
  final PageEvent pageEvent;

  OnSaldoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class OnSetupDateTimePage extends PageState {
  final PageEvent pageEvent;

  OnSetupDateTimePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}
