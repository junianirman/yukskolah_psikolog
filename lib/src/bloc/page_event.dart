part of 'page_bloc.dart';

abstract class PageEvent extends Equatable {
  const PageEvent();
}

class GoToSplashPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToSignInPage extends PageEvent {
  @override
  List<Object> get props => [];
}

class GoToMainPage extends PageEvent {
  final int bottomNavBarIndex;

  GoToMainPage({this.bottomNavBarIndex = 0});

  @override
  List<Object> get props => [bottomNavBarIndex];
}

class GoToChatPage extends PageEvent {
  final PageEvent pageEvent;

  GoToChatPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToProfileDetailPage extends PageEvent {
  final User user;
  final PageEvent pageEvent;

  GoToProfileDetailPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class GoToAppoinmentPage extends PageEvent {
  final PageEvent pageEvent;

  GoToAppoinmentPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToReschedulePage extends PageEvent {
  final Consultation consultation;
  final PageEvent pageEvent;

  GoToReschedulePage(this.consultation, this.pageEvent);

  @override
  List<Object> get props => [consultation, pageEvent];
}

class GoToSchedulePage extends PageEvent {
  final PageEvent pageEvent;

  GoToSchedulePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToUploadPage extends PageEvent {
  final PageEvent pageEvent;

  GoToUploadPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToUploadFilePage extends PageEvent {
  final PageEvent pageEvent;

  GoToUploadFilePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToSetupDatePage extends PageEvent {
  final PageEvent pageEvent;

  GoToSetupDatePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToChangePasswordPage extends PageEvent {
  final User user;
  final PageEvent pageEvent;

  GoToChangePasswordPage(this.user, this.pageEvent);

  @override
  List<Object> get props => [user, pageEvent];
}

class GoToSaldoPage extends PageEvent {
  final PageEvent pageEvent;

  GoToSaldoPage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}

class GoToSetupDateTimePage extends PageEvent {
  final PageEvent pageEvent;

  GoToSetupDateTimePage(this.pageEvent);

  @override
  List<Object> get props => [pageEvent];
}
