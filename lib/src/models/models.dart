import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'event.dart';
part 'inbox.dart';
part 'user.dart';
part 'follow.dart';
part 'consultation.dart';
part 'upload.dart';
part 'setupdate.dart';
part 'saldo.dart';
part 'bank.dart';
