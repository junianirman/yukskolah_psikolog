part of 'models.dart';

class Follow extends Equatable {
  final int follower;
  final int following;

  Follow({this.follower, this.following});

  @override
  List<Object> get props => [follower, following];
}
