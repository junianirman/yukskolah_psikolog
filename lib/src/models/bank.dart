part of 'models.dart';

class Bank extends Equatable {
  final int id;
  final String name;

  Bank({this.id, this.name});

  @override
  List<Object> get props => [id, name];
}
