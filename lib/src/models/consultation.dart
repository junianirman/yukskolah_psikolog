part of 'models.dart';

enum ConsultationStatus { waiting, reschedule, inschedule }

class Consultation extends Equatable {
  final int id;
  final String name;
  final String consultationType;
  final DateTime scheduleDate;
  final DateTime scheduleTime;
  final DateTime createDate;
  final ConsultationStatus status;
  final String picturePath;

  Consultation(
      {this.id,
      this.name,
      this.consultationType,
      this.scheduleDate,
      this.scheduleTime,
      this.createDate,
      this.status,
      this.picturePath});

  @override
  List<Object> get props => [
        id,
        name,
        consultationType,
        scheduleDate,
        scheduleTime,
        createDate,
        status,
        picturePath
      ];
}

List<Consultation> dummyConsultation = [
  Consultation(
      id: 1,
      name: "Ananda Anak Pertama",
      consultationType: "Psychological Test",
      scheduleDate: DateTime.now(),
      scheduleTime: DateTime.now(),
      createDate: DateTime.now(),
      status: ConsultationStatus.waiting,
      picturePath: 'assets/user_pic.jpg'),
  Consultation(
      id: 2,
      name: "Ananda Anak Pertama",
      consultationType: "Psychological Test",
      scheduleDate: DateTime.now(),
      scheduleTime: DateTime.now(),
      createDate: DateTime.now(),
      status: ConsultationStatus.inschedule,
      picturePath: 'assets/user_pic.jpg'),
  Consultation(
      id: 3,
      name: "Adinda Adik Kedua",
      consultationType: "Psychological Test",
      scheduleDate: DateTime.now(),
      scheduleTime: DateTime.now(),
      createDate: DateTime.now(),
      status: ConsultationStatus.reschedule,
      picturePath: 'assets/user_pic.jpg')
];
