part of 'models.dart';

enum SaldoTab { kosultasi, penarikan }

class Saldo extends Equatable {
  final int id;
  final int balance;
  final String picturePath;
  final String description;
  final List<Bank> bank;
  final SaldoTab saldoTab;

  Saldo({
    this.id,
    this.balance,
    this.picturePath,
    this.description,
    this.bank,
    this.saldoTab,
  });

  @override
  List<Object> get props => [id, balance, picturePath, description, bank];
}

List<Saldo> dummySaldo = [
  Saldo(
      id: 1,
      balance: 0,
      picturePath: 'assets/wallet.png',
      description: "Belum Ada Riwayat Transaksi",
      bank: [],
      saldoTab: SaldoTab.kosultasi),
  Saldo(
      id: 2,
      balance: 0,
      picturePath: 'assets/wallet.png',
      description: "Belum Ada Riwayat Transaksi",
      bank: [
        Bank(id: 1, name: "BCA"),
        Bank(id: 2, name: "BRI"),
        Bank(id: 3, name: "BNI"),
        Bank(id: 4, name: "BNI SYARIAH"),
        Bank(id: 5, name: "MANDIRI"),
      ],
      saldoTab: SaldoTab.penarikan),
];
