part of 'models.dart';

class Upload extends Equatable {
  final int id;
  final String name;
  final String description;

  Upload({this.id, this.name, this.description});

  @override
  List<Object> get props => [id, name, description];
}

List<Upload> dummyUpload = [
  Upload(id: 1, name: "Form Test 1", description: "Form Test IQ"),
  Upload(id: 2, name: "Form Test 2", description: "Form Test IQ - 2"),
  Upload(id: 3, name: "Form Test 3", description: "Form Test IQ - 3")
];
