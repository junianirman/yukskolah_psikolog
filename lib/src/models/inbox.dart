part of 'models.dart';

enum InboxType { school, loan, consultation, elearning, shopping }

class Inbox extends Equatable {
  final int id;
  final String picturePath;
  final String store;
  final DateTime dateTime;
  final String chat;
  final InboxType inboxType;

  Inbox(
      {this.id,
      this.picturePath,
      this.store,
      this.dateTime,
      this.chat,
      this.inboxType});

  @override
  List<Object> get props => [id, picturePath, store, dateTime];
}

List<Inbox> dummyInbox = [
  Inbox(
      id: 1,
      picturePath: "assets/user_pic.jpg",
      store: "Ananda Anak Pertama",
      dateTime: DateTime.now(),
      chat: "hallo,selamat siang?",
      inboxType: InboxType.shopping),
  // Inbox(
  //     id: 2,
  //     picturePath: "assets/user_pic.jpg",
  //     store: "Betta Store",
  //     dateTime: DateFormat('yyyy-MM-dd').format(DateTime.now()),
  //     chat: "can be delivered today?",
  //     inboxType: InboxType.shopping)
];
