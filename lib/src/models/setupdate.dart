part of 'models.dart';

class SetupDate extends Equatable {
  final int id;
  final DateTime startTime;
  final DateTime endTime;
  final DateTime day;

  SetupDate({this.id, this.startTime, this.endTime, this.day});

  @override
  List<Object> get props => [id, startTime, endTime, day];
}

List<SetupDate> dummySetupDate = [
  SetupDate(
    id: 1,
    startTime: DateTime.now(),
    endTime: DateTime.now().add(Duration(minutes: 30)),
    day: DateTime.now(),
  ),
  SetupDate(
    id: 2,
    startTime: DateTime.now(),
    endTime: DateTime.now().add(Duration(minutes: 30)),
    day: DateTime.now(),
  ),
  SetupDate(
    id: 3,
    startTime: DateTime.now(),
    endTime: DateTime.now().add(Duration(minutes: 30)),
    day: DateTime.now(),
  ),
];
